<?php
namespace App\Consumer;

use App\EventSubscriber\DomainEventBroadcaster;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

class EmailNotificationConsumer implements ConsumerInterface
{
    private $eventBroadcaster;

    public function __construct(DomainEventBroadcaster $broadcaster)
    {
        $this->eventBroadcaster = $broadcaster;
    }

    public function execute(AMQPMessage $msg)
    {
        $this->eventBroadcaster->handle($msg->get('routing_key'), $msg->getBody());
    }
}