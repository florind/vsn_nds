<?php
namespace App\EventSubscriber;

interface DomainEventSubscriberInterface
{
    public function canHandle(string $eventType);

    public function handle(string $payload);
}