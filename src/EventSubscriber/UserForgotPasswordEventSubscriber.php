<?php
namespace App\EventSubscriber;

use Symfony\Component\Translation\TranslatorInterface;

class UserForgotPasswordEventSubscriber implements DomainEventSubscriberInterface
{
    const KEY = 'user.forgot_password';

    private $mailer;
    private $templateEngine;
    private $translator;

    public function __construct(
        \Swift_Mailer $mailer,
        \Twig_Environment $templateEngine,
        TranslatorInterface $translator
    ) {
        $this->mailer = $mailer;
        $this->translator = $translator;
        $this->templateEngine = $templateEngine;
    }

    public function canHandle(string $eventType)
    {
        return $eventType === self::KEY;
    }

    public function handle(string $payload)
    {
        $data = json_decode($payload, true);

        $subject = $this->translator->trans('forgot_password.subject', [], 'emails', $data['locale']);
        $message = (new \Swift_Message($subject))
            ->setTo($data['email'])
            ->setBody($this->templateEngine->render(
                'email/forgot_password.html.twig',
                [
                    'email' => $data['email'],
                    'token' => $data['reset_token'],
                    'locale' => $data['locale']
                ]
            ), 'text/html');

        $this->mailer->send($message);
    }
}