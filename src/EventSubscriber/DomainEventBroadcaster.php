<?php
namespace App\EventSubscriber;

class DomainEventBroadcaster
{
    private $subscribers = [];

    public function addSubscriber(DomainEventSubscriberInterface $subscriber)
    {
        $this->subscribers[] = $subscriber;
    }

    public function handle(string $eventType, string $payload)
    {
        foreach ($this->subscribers as $subscriber) {
            if ($subscriber->canHandle($eventType)) {
                $subscriber->handle($payload);
                return;
            }
        }
    }
}